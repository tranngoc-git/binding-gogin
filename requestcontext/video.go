package requestcontext

import "github.com/gin-gonic/gin"

// Video ..
type Video struct {
	Header *videoHeader
	Query  *videoQuery
	URL    string
}

// accquire implement interface
var (
	_ rcValidator = &Video{}
	_ builder     = &Video{}
)

// NewVideo return video object
func NewVideo(c *gin.Context) (*Video, error) {
	// binding
	var v = &Video{}

	err := newDirector(v).build(c)
	if err != nil {
		return nil, err
	}

	// validate
	err = v.execValidate()
	if err != nil {
		return nil, err
	}

	return v, nil
}

func (v *Video) setHeader(c *gin.Context) error {
	var err error

	v.Header, err = newVideoHeader(c)
	if err != nil {
		return err
	}

	return nil
}

func (v *Video) setQuery(c *gin.Context) error {
	var err error

	v.Query, err = newVideoQuery(c)
	if err != nil {
		return err
	}

	return nil
}

func (v *Video) execValidate() error {
	return execValidate(v, validateURL)
}

func validateURL(i interface{}) error {
	v := i.(*Video)

	if v.URL == "" {
		v.URL = "facebook.com"
	}

	return nil
}
