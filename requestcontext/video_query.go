package requestcontext

import "github.com/gin-gonic/gin"

type videoQuery struct {
	Device string `form:"d"`
	Height string `form:"h"`
	Weight string `form:"w"`
}

var _ rcValidator = &videoQuery{}

func newVideoQuery(c *gin.Context) (*videoQuery, error) {
	// binding
	videoQuery := &videoQuery{}
	err := c.ShouldBindQuery(videoQuery)
	if err != nil {
		return nil, err
	}

	// validate
	err = execValidate(videoQuery, validateDevice)
	if err != nil {
		return nil, err
	}

	return videoQuery, nil
}

func (v *videoQuery) execValidate() error {
	return execValidate(v, validateDevice)
}

func validateDevice(i interface{}) error {
	v := i.(*videoQuery)

	if v.Device == "" {
		v.Device = "pc"
	}

	return nil
}
